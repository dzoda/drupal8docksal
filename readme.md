Required: Docker
If you have docker installed on your pc you need just to install docksal.

Open terminal and hit : curl -fsSL get.docksal.io | bash

Clone this repo and move into docroot folder. Type fin init and that's it.

If db folder is empty, docksal will install vanilla drupal8, otherwise db will be imported during site init.

Bash script will do next:

Create settings.php file
Create files folder
Set correct permissions on folders
Database import
Config import
Drush updb
Drush cr
